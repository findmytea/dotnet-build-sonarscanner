# Dotnet Build Sonar Scanner #

An Docker environment in which to build dotnet core applications and run sonar scanner

### Docker Image ###

The Docker image is available here:

[https://hub.docker.com/r/pjgrenyer/dotnet-build-sonarscanner](https://hub.docker.com/r/pjgrenyer/dotnet-build-sonarscanner)

### How do I get set up? ###

* docker pull pjgrenyer/dotnet-build-sonarscanners

